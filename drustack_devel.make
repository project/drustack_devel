api = 2
core = 7.x

; Modules
projects[devel_themer][download][branch] = 7.x-1.x
projects[devel_themer][download][type] = git
projects[devel_themer][subdir] = contrib
projects[examples][download][branch] = 7.x-1.x
projects[examples][download][type] = git
projects[examples][subdir] = contrib
projects[schema][download][tag] = 7.x-1.2
projects[schema][download][type] = git
projects[schema][subdir] = contrib
